// Hamiltonian.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include<vector>
#include "Node.h"
#include "Hypercube.h"
#include <iostream>

int main()
{

	HyperCube cube(3);
	for (int i = 0; i < cube.Vertices().size(); i++)
	{
		for (int g = 0; g < cube.Vertices()[i].Vertex().size(); g++)
		{
			std::cout << cube.Vertices()[i].Vertex()[g];
		}
		std::cout << endl;
		
		set<Node*>::iterator it = cube.Vertices()[i].Relatives().begin();
		while (it != cube.Vertices()[i].Relatives().end())
		{
			std::cout << "\t";
			for (int v = 0; v < (*it)->Vertex().size(); v++)
			{
				std::cout << (*it)->Vertex()[v];
			}
			std::cout << endl;
			it++;
		}

		std::cout << endl;
	}

	set<vector<Node*>> pathList;
	vector<Node*> temp;

	for (int i = 0; i < cube.Vertices().size(); i++)
	{
		Node* ptr;
		ptr = &(cube.Vertices()[i]);
		temp.push_back(ptr);
		cube.GetPaths(pathList,temp);
		temp.clear();
	}

	set<vector<Node*>>::iterator pathIterator = pathList.begin();
	std:cout << pathList.size();
	std::cout << endl;
	while (pathIterator != pathList.end())
	{
		for (int i = 0; i < (*pathIterator).size(); i++)
		{
			for (int v = 0; v < (*pathIterator)[i]->Vertex().size(); v++)
			{
				std::cout << (*pathIterator)[i]->Vertex()[v];
			}
			std::cout << endl;
		}
		std::cout << endl;
		pathIterator++;
	}

	std::cout << endl;


	cin.get();
	return 0;
}
