#pragma once
#include "Node.h"

class HyperCube
{
	private:
		vector<Node> vertices;

	public:
		HyperCube(int dimension);
		~HyperCube();
		vector<Node> &Vertices();
		void GetPaths(set<vector<Node*>> &pathList, vector<Node*> &currentPath);//, set<Node*> visited);
};

