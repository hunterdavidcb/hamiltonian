#include<vector>
#include<set>
#include<iostream>
#pragma once

using namespace std;

class Node
{
	private:
		vector<bool> vertex;
		set<Node*> relatives;
		int dimension;
	public:
		Node(int number, int dimension);
		~Node();
		const vector<bool> Vertex()const;
		const bool operator[](int);
		const set<Node*> &Relatives(void)const;
		const void SetRelatives(const set<Node*> rel);
		bool DiffersByOne(const Node node);
		
		/*friend ostream& operator<<(ostream& os, const set<vector<bool>> rel);
		friend ostream& operator<<(ostream& os, const vector<bool>& rel);*/
};