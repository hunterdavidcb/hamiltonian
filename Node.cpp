#include "pch.h"
#include "Node.h"

Node::Node(int number, int dimension)
{
	if (number > -1 && number < pow(2,dimension))
	{
		while (number != 0)
		{
			vertex.push_back(number % 2 == 0 ? false : true);
			number /= 2;
		}

		if (vertex.size() < dimension)
		{
			int diff = dimension - vertex.size();
			for (int i = 0; i < diff; i++)
			{
				vertex.push_back(false);
			}
		}

		std::reverse(vertex.begin(), vertex.end());

		/*vector<bool> copy(vertex);
		for (int i = 0; i < dimension; i++)
		{
			vector<bool> copy(vertex);
			copy[i] = !copy[i];
			relatives.emplace(copy);
		}*/

	}
	
}

Node::~Node()
{

}

const vector<bool> Node::Vertex()const
{
	return vertex;
}

const bool Node::operator[](int index)
{
	if (index > -1 && index < vertex.size())
	{
		return vertex[index];
	}
	else
	{
		throw std::range_error("index was out of range");
	}
}

const set<Node*> &Node::Relatives(void)const
{
	return this->relatives;
}

const void Node::SetRelatives(const set<Node*> rel)
{
	relatives = rel;
}

bool Node::DiffersByOne(const Node node)
{
	if (vertex.size() != node.Vertex().size())
	{
		return false;
	}

	int dif = 0;
	for (int i = 0; i < vertex.size(); i++)
	{
		if (vertex[i] != node.Vertex()[i])
		{
			dif++;
		}
	}

	return dif == 1;
	
}



//ostream& operator<<(ostream& os, const set<vector<bool>> rel)
//{
//	// TODO: insert return statement here
//	set<vector<bool>>::iterator it = rel.begin();
//	while (it != rel.end())
//	{
//		for (int g = 0; g < (*it).size(); g++)
//		{
//			os << (*it)[g];
//		}
//		os << endl;
//		it++;
//	}
//
//	return os;
//}
//
//ostream & operator<<(ostream & os, const vector<bool>& rel)
//{
//	// TODO: insert return statement here
//	for (int i = 0; i < rel.size(); i++)
//	{
//		os << rel[i];
//	}
//
//	return os;
//}
