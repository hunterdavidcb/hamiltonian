#include "pch.h"
#include "HyperCube.h"


HyperCube::HyperCube(int dimension)
{
	int numberOfVertices = pow(2, dimension);
	for (int i = 0; i < numberOfVertices; i++)
	{
		Node temp(i, dimension);
		vertices.push_back(temp);
	}

	for (int i = 0; i < vertices.size(); i++)
	{
		set<Node*> rel;
		for (int g = 0; g < vertices.size(); g++)
		{
			if (g == i)
			{
				continue;
			}
			else
			{
				if (vertices[i].DiffersByOne(vertices[g]))
				{
					Node* ptr;
					ptr = &vertices[g];
					rel.emplace(ptr);
				}
			}
		}
		vertices[i].SetRelatives(rel);
	}
}


HyperCube::~HyperCube()
{
}

vector<Node> &HyperCube::Vertices()
{
	return this->vertices;
}

void HyperCube::GetPaths(set<vector<Node*>>& pathList, vector<Node*>& currentPath)
{
	int currentSize, sizeAfterRecursion;
	currentSize = currentPath.size();

	//the current path is too short
	if (currentPath.size() < vertices.size())
	{
		set<Node*> relativesOfCurrentNode = (*currentPath[currentSize-1]).Relatives();
		set<Node*>::iterator nodeIterator = relativesOfCurrentNode.begin();

		while (nodeIterator != relativesOfCurrentNode.end())
		{
			if (find(currentPath.begin(),currentPath.end(),*nodeIterator) == currentPath.end())
			{
				currentPath.push_back(*nodeIterator);
				GetPaths(pathList, currentPath);
				sizeAfterRecursion = currentPath.size();
				for (int i = 0; i < (sizeAfterRecursion - currentSize); i++)
				{
					currentPath.pop_back();
				}
			}
			nodeIterator++;
		}

	}
	else if (currentPath.size() == vertices.size())
	{
		pathList.emplace(currentPath);
	}
}
